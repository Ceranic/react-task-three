import React, {useState} from 'react';
import './App.css';
import Input from './components/Input'
import List from './components/ListComponents'
import Button from './components/Button';

function App() {

  const [task,setTask] = useState('');
  const [allTasks, setAllTasks] = useState(['Uradi to', 'Nesto drugo']);

  return (
    <div className="App">
      <Input task={task} setTask={setTask}></Input>
      <List className="lista" allTasks={allTasks} ></List>
      <Button task={task} setTask={setTask} allTasks={allTasks} setAllTasks={setAllTasks}></Button>
    </div>
  );
}

export default App;