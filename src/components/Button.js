import React from 'react'

function Button(props) {

    const addTask = () => {
        if(props.task.replace(/ /g, '') !== ''){
            props.setAllTasks([...props.allTasks, props.task]);
            props.setTask('');
        }
    }

    return (
        <div>
            <button className="button" onClick={addTask}>Add task</button>
        </div>
    )
}

export default Button
