import React from 'react'
import "./Input.css"

function Input(props) {


    

    return (
        <div className="inputMain">
            <input placeholder="Enter task..." value={props.task} onChange={(e) => props.setTask(e.target.value)} type="text"/>
        </div>
    )
}

export default Input
