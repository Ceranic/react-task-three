import React from 'react'

function ListItem(props) {
  return (
    <div className="listItems">
      <span>{props.id}#</span>{props.task}
    </div>
  )
}

export default ListItem
