import React from 'react'
import ListItem from './ListItem'
import './ListComponents.css'

function ListComponents(props) {
    return (
        <div className="listName">
            <div className="listNameInner">
                {props.allTasks.map((x,y) => <ListItem key={y} id={y+1} task={x}/>)}
            </div>
        </div>
    )
}

export default ListComponents
